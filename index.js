/*jshint node:true*/
"use strict";

var express = require("express");
var app = express();
var bodyParser = require("body-parser");
// var favicon = require("serve-favicon");
var logger = require("morgan");
var port = process.env.PORT || 4200;
var four0four = require("./utils/404")();
var path = require("path");

var rootPath = path.normalize(__dirname + "/./");

var environment = process.env.NODE_ENV;

// app.use(favicon(__dirname + "/favicon.png"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logger("dev"));

console.log("About to crank up node");
console.log("PORT=" + port);
console.log("NODE_ENV=" + environment);

console.log("** RUN **");
app.use(express.static(rootPath + "/app"));
app.use(express.static(rootPath + "/"));
app.use(express.static(rootPath + "/tmp"));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With"
  );
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
  next();
});
// Any invalid calls for templateUrls are under app/* and should return 404
app.use("/app/*", function(req, res, next) {
  four0four.send404(req, res);
});
// Any deep link calls should return index.html
app.use("/*", express.static(rootPath + "/app/index.html"));

app.listen(port, function() {
  console.log("Express server listening on port " + port);
  console.log(
    "env = " +
      app.get("env") +
      "\n__dirname = " +
      __dirname +
      "\nprocess.cwd = " +
      process.cwd()
  );
});
