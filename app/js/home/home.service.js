(function() {
  'use strict';

  angular.module('app.home').factory('HomeService', homeService);

  homeService.$inject = [
    '$rootScope',
    '$window',
    'EVENTS',
    'HomeResource',
    '$state',
    'Helper',
    'toastr',
    '$translate',
    'jwtHelper'
  ];
  function homeService(
    $rootScope,
    $window,
    EVENTS,
    HomeResource,
    $state,
    Helper,
    toastr,
    $translate,
    jwtHelper
  ) {
    var homeService = {};

    homeService.getProducts = function(success, error) {
        HomeResource.products()
          .$promise.then(function(response) {
            console.log({ response });
            success(response.data);
          })
          .catch(function(e) {
            console.log({ e });
            error(e);
          })
          .catch(function(response) {
            //unsuccessful get setting, fire login failed event for bad request.
            $rootScope.$broadcast(EVENTS.badRequest);
            $window.sessionStorage.removeItem('x-access-token');
            $window.sessionStorage.removeItem('userInfo');
          });
      };


    return homeService;
  }
})();
