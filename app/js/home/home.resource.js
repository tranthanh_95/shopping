(function() {
  'use strict';

  angular.module('app.home').factory('HomeResource', homeResource);

  homeResource.$inject = ['$resource', 'Helper'];
  function homeResource($resource, Helper) {
    return {
      products: function() {
        var resourceGetProducts = $resource(Helper.getApiUrl('/products'),
          {},
          {
            get: {
              method: 'GET',
              // headers: { 'x-access-token': Helper.getToken() }
            }
          });
        return resourceGetProducts.get();
      },

    };
  }
})();
