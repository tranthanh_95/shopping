(function() {
  "use strict";

  angular
    .module("app.home")

    // routing
    .config(routing);

  routing.$inject = ["$stateProvider", "$urlRouterProvider"];
  function routing($stateProvider, $urlRouterProvider) {
    // For any unmatched url, redirect to root
    $urlRouterProvider.otherwise("/404");

    $stateProvider
     .state('home', {
        url: '/',
        views: {
          header: {
            templateUrl: 'templates/partials/header.html'
          },
          '': {
            templateUrl: 'templates/home/index.html',
            controller: 'HomeController',
            controllerAs: 'HomeCtrl'
          },
          footer: {
            templateUrl: 'templates/partials/footer.html'
          }
        }
      })
  }
})();
