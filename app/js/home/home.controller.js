(function() {
  'use strict';

  angular.module('app.auth').controller('HomeController', HomeController);

  HomeController.$inject = [
    'toastr',
    '$scope',
    '$translate',
    'Helper',
    'HomeService',
    '$state'
  ];

  function HomeController(
    toastr,
    $scope,
    $translate,
    Helper,
    HomeService,
    $state
  ) {
    try {
      var LoginCtrl = this;
      HomeService.getProducts(
          function(response) {
            console.log('Data response from server: ', response);
          },
          function(err) {
            toastr.error($translate.instant(err.data.message));
          }
        );

    } catch (e) {
      $scope.loading = false;
      toastr.error($translate.instant('bad_request'));
    }
  }
})();
