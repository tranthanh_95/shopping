(function() {
  'use strict';

  angular.module('app.auth').factory('AuthService', authService);

  authService.$inject = [
    '$rootScope',
    '$window',
    'EVENTS',
    'AuthResource',
    '$state',
    // 'ProfileService',
    'Helper',
    'toastr',
    '$translate',
    'jwtHelper'
  ];
  function authService(
    $rootScope,
    $window,
    EVENTS,
    AuthResource,
    $state,
    // ProfileService,
    Helper,
    toastr,
    $translate,
    jwtHelper
  ) {
    var authService = {};

    //the login function
    // authService.login = function(user, success, error) {
    //   AuthResource.login(user)
    //     .$promise.then(function(responseLogin) {
    //       //set the browser session, to avoid relogin on refresh
    //       $window.sessionStorage['x-access-token'] = responseLogin.data;

    //       // get user info
    //       ProfileService.getUserInfo()
    //         .$promise.then(function(responseMe) {
    //           $window.sessionStorage['userInfo'] = JSON.stringify(
    //             responseMe.data
    //           );
    //           //fire event of successful login
    //           $rootScope.$broadcast(EVENTS.loginSuccess);
    //           //run success function
    //           success();
    //         })
    //         .catch(function(e) {
    //           toastr.error($translate.instant(e.data.message));
    //           error();
    //         })
    //         .catch(function(e) {
    //           this.removeToken('x-access-token');
    //           this.removeUserInfo('userInfo');
    //           $rootScope.$broadcast(EVENTS.loginFailed);
    //         });
    //     })
    //     .catch(function(e) {
    //       toastr.error($translate.instant(e.data.message));
    //       error();
    //     })
    //     .catch(function(e) {
    //       $rootScope.$broadcast(EVENTS.loginFailed);
    //       this.removeToken('x-access-token');
    //       this.removeUserInfo('userInfo');
    //     });
    // };

    //check if the user is authenticated
    authService.isAuthenticated = function() {
      return (
        !!this.getToken() && !!this.getUserInfo() && !this.isExpiredToken()
      );
    };

    /**
     * tell is token is expired (compared to now)
     *
     * @param {string} encodedToken - base 64 token received from server and stored in local storage
     * @returns {bool} returns true if expired else false
     */
    authService.isExpiredToken = function() {
      try {
        var encodedToken = this.getToken();
        if (!encodedToken) return true;
        return jwtHelper.isTokenExpired(encodedToken);
      } catch (e) {
        return true;
      }
    };

    //log out the user and broadcast the logoutSuccess event
    authService.logout = function(success, error) {
      try {
        this.removeToken('x-access-token');
        this.removeUserInfo('userInfo');
        success();
      } catch (exception) {
        error();
      }
    };

    authService.getToken = function() {
      return $window.sessionStorage.getItem('x-access-token') || null;
    };

    authService.removeToken = function() {
      return $window.sessionStorage.removeItem('x-access-token');
    };

    authService.getUserInfo = function() {
      return $window.sessionStorage.getItem('userInfo') || null;
    };

    authService.removeUserInfo = function() {
      return $window.sessionStorage.removeItem('userInfo');
    };

    return authService;
  }
})();
