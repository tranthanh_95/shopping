(function() {
  'use strict';

  angular.module('app.auth').factory('AuthResource', authResource);

  authResource.$inject = ['$resource', 'Helper'];
  function authResource($resource, Helper) {
    return {
      login: function(data) {
        var resourceLogin = $resource(Helper.getApiUrl('/login'));
        return resourceLogin.save(data);
      },

      register: function(data) {
        var resourceRegister = $resource(Helper.getApiUrl('/register'));
        return resourceRegister.save(data);
      },


    };
  }
})();
