(function() {
  'use strict';

  angular
    .module('app.auth')

    // routing
    .config(routing);

  routing.$inject = ['$stateProvider', '$urlRouterProvider'];
  function routing($stateProvider, $urlRouterProvider) {
    // For any unmatched url, redirect to root
    // $urlRouterProvider.otherwise('/404');

    $stateProvider
     .state('login', {
        url: '/login',
        views: {
          header: {
            templateUrl: 'templates/partials/header.html'
          },
          '': {
            // templateUrl: 'templates/login/index.html',
            controller: 'LoginController',
            controllerAs: 'LoginCtrl'
          },
          footer: {
            templateUrl: 'templates/partials/footer.html'
          }
        }
      })
      // .state('login', {
      //   parent: 'master-login',
      //   url: '/login',
      //   views: {
      //     'login-left': {
      //       templateUrl: 'templates/login/index.html',
      //       controller: 'LoginController',
      //       controllerAs: 'LoginCtrl'
      //     }
      //   }
      // })
      
  }
})();
