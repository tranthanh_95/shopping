(function() {
  'use strict';

  angular.module('app.auth').controller('LoginController', LoginController);

  LoginController.$inject = [
    'toastr',
    '$scope',
    '$translate',
    'Helper',
    'envService',
    '$state'
  ];

  function LoginController(
    toastr,
    $scope,
    $translate,
    Helper,
    envService,
    $state
  ) {
    try {
      var LoginCtrl = this;

      var authUrl = envService.read('authUrl');

      console.log({ authUrl });

    } catch (e) {
      $scope.loading = false;
      toastr.error($translate.instant('bad_request'));
    }
  }
})();
