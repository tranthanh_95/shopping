(function() {
  "use strict";

  angular.module("app.core").constant("EVENTS", {
    loginSuccess: "auth-login-success",
    loginFailed: "auth-login-failed",
    logoutSuccess: "auth-logout-success",
    sessionTimeout: "auth-session-timeout",
    notAuthenticated: "auth-not-authenticated",
    notAuthorized: "auth-not-authorized",
    serverError: "server-error",
    notFound: "not-found",
    badRequest: "bad-request"
  });
})();
