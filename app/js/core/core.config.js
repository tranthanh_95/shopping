(function() {
  'use strict';

  angular
    .module('app.core')
    .config(authInterceptor)
    .config(environment)
    .config(translations)
    .run(auth)
    .run(language);

  /* Adding the auth interceptor here, to check every $http request*/
  authInterceptor.$inject = ['$httpProvider'];
  function authInterceptor($httpProvider) {
    $httpProvider.interceptors.push([
      '$injector',
      function($injector) {
        return $injector.get('AuthInterceptor');
      }
    ]);
  }

  /* Config environment*/
  environment.$inject = ['envServiceProvider'];
  function environment(envServiceProvider) {
    // set the domains and variables for each environment
    envServiceProvider.config({
      domains: {
        development: ['localhost'],
        production: ['acme.com'],
        staging: ['test.acme.com']
      },
      vars: {
        development: {
          apiUrl: 'http://192.168.1.131:8765/api/v1/user',
          authUrl: 'http:192.168.1.131:3000/auth/google'
        },
        staging: {
          apiUrl: 'CI_UP_API_URL_STG'
        },
        production: {
          apiUrl: 'http://52.199.22.188:1234/api/v1/user'
        },
        defaults: {
          apiUrl: '//api.default.com/v1',
          staticUrl: '//static.default.com'
        }
      }
    });

    // run the environment check, so the comprobation is made
    // before controllers and services are built
    envServiceProvider.check();
    envServiceProvider.set('development');
  }

  /* Config translate*/
  translations.$inject = ['$translateProvider'];
  function translations($translateProvider) {
    $translateProvider
      .useStaticFilesLoader({
        prefix: '/translations/locale-',
        suffix: '.json'
      })
      .preferredLanguage('en')
      .useLocalStorage();
    $translateProvider.useSanitizeValueStrategy('sceParameters');
  }

  /* Config auth*/
  auth.$inject = [
    '$rootScope',
    'AuthService',
    'EVENTS',
    '$location',
    'Helper',
    '$state',
    '$timeout'
  ];
  function auth(
    $rootScope,
    AuthService,
    EVENTS,
    $location,
    Helper,
    $state,
    $timeout
  ) {
    $rootScope.$on('$locationChangeSuccess', function locationChangeSuccess(
      event
    ) {
      event.preventDefault();
      var nextLocation = $location.path();
      $timeout(function() {
        var stateName = $state.current.name;
        if (
          nextLocation != '/400' &&
          nextLocation != '/500' &&
          nextLocation != '/403' &&
          nextLocation != '/404'
        ) {
          // if (!AuthService.isAuthenticated()) {
          //   $rootScope.$broadcast(EVENTS.notAuthenticated);
          // } else {
            // if (nextLocation == '/login') {
            //   if (AuthService.isAuthenticated()) {
            //     $state.go('wallet');
            //   }
            //   AuthService.checkLoginPage();
            // } else {
            //   AuthService.checkPolicy(function() {}, function() {});
            //   AuthService.changeUserInfo();
            // }

            // $rootScope.fullname = Helper.getUserEachInfo('fullname');
            // $rootScope.email = Helper.getUserEachInfo('email');
          // }
        }
      }, 100);
    });
  }

  /* Config locale follow by language*/
  language.$inject = ['$rootScope', 'Helper', 'amMoment'];
  function language($rootScope, Helper, amMoment) {
    $rootScope.lang = 'ja';
    if (Helper.isLocale('en')) {
      amMoment.changeLocale('en');
    } else {
      amMoment.changeLocale('ja');
    }

    $rootScope.time = new Date();
  }

  var lang = window.localStorage.getItem('NG_TRANSLATE_LANG_KEY') || 'en';
})();
