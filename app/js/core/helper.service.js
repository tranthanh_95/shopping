(function() {
  'use strict';

  angular.module('app.core').factory('Helper', helper);

  helper.$inject = ['envService', '$window'];
  function helper(envService, $window) {
    var apiUrl = envService.read('apiUrl');
    return {
      getApiUrl: function(uri) {
        return apiUrl + uri;
      },

      getToken: function() {
        return $window.sessionStorage.getItem('x-access-token') || 'a8X83z0sNvAFEoFbJAtDEfiq1gu18PJ5kjswSFZVXzVZSzf4tHoi1wn2Yik3EB43';
      },

      getUserEachInfo: function(field) {
        var user =
          $window.sessionStorage.getItem('userInfo') || JSON.stringify({});
        user = JSON.parse(user);
        return user[field];
      },

      getLocale: function() {
        return $window.localStorage.getItem('NG_TRANSLATE_LANG_KEY') || null;
      },

      isLocale: function(locale) {
        if (!locale) return false;
        var currentLocale = this.getLocale();
        if (currentLocale == locale) return true;
        return false;
      },

      transferObjectToArray: function(object) {
        var arr = [];
        if (!object) return arr;
        Object.keys(object).some(function(key) {
          if (object[key]) arr.push(key);
        });
        return arr;
      }
    };
  }
})();
