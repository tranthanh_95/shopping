(function() {
  "use strict";
  /**
   * This interceptor will make sure that, after each $http request
   * if the user doesn't have access to something runs the according
   * event, given the response status codes from the server.
   */
  angular.module("app.core").factory("AuthInterceptor", [
    "$rootScope",
    "$q",
    "EVENTS",
    function($rootScope, $q, EVENTS) {
      return {
        responseError: function(response) {
          $rootScope.$broadcast(
            {
              401: EVENTS.notAuthenticated,
              403: EVENTS.notAuthorized,
              419: EVENTS.sessionTimeout,
              440: EVENTS.sessionTimeout,
              500: EVENTS.serverError,
              400: EVENTS.badRequest,
              404: EVENTS.notFound
            }[response.status],
            response
          );

          $rootScope.$broadcast(
            {
              // 1: EVENTS.notVerifyEmail,
              // 2: EVENTS.notVerifyPhone,
              // 6: EVENTS.accountBlocked,
              // 5: EVENTS.notInitWallet,
              // 28: EVENTS.notUploadIdentification,
              // 31: EVENTS.notVerifyIdentification
            }[response.data.error],
            response
          );

          return $q.reject(response);
        }
      };
    }
  ]);
})();
