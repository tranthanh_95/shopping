(function() {
  'use strict';

  angular.module('app.core', [
    'environment',
    'ui.router',
    'ngResource',
    'ngCookies',
    'ngSanitize',
    'pascalprecht.translate',
    'ngAnimate',
    'angularMoment',
    'toastr',
    'ui.bootstrap',
    'cp.ngConfirm',
    'ngMessages',
    'angular-jwt'
  ]);
})();
