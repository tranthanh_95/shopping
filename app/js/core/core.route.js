(function() {
  'use strict';

  angular
    .module('app.core')

    // routing
    .config(routing);

  routing.$inject = [
    '$stateProvider',
    '$locationProvider',
    '$urlRouterProvider'
  ];
  function routing($stateProvider, $locationProvider, $urlRouterProvider) {
    // $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    // For any unmatched url, redirect to root
    $urlRouterProvider.otherwise('/404');

    $stateProvider
      // .state('master', {
      //   abstract: true,
      //   templateUrl: 'index.html'
      // })
      // .state('master-layout', {
      //   abstract: true,
      //   views: {
      //     header: {
      //       templateUrl: 'templates/partials/header.html'
      //     },
      //     '': {
      //       templateUrl: 'templates/layout-master.html'
      //     },
      //      footer: {
      //       templateUrl: 'templates/partials/footer.html'
      //     }
      //   }
      // })
     
      // .state('login', {
      //   abstract: true,
      //   views: {
      //     header: {
      //       templateUrl: 'templates/partials/header.html'
      //     },
      //     '': {
      //       templateUrl: 'templates/login/index.html'
      //     },
      //     footer: {
      //       templateUrl: 'templates/partials/footer.html'
      //     }
      //   }
      // })
      .state('404', {
        url: '/404',
        templateUrl: 'templates/partials/errors/404.html'
      })
      .state('400', {
        url: '/400',
        templateUrl: 'templates/partials/errors/400.html'
      })
      .state('403', {
        url: '/403',
        templateUrl: 'templates/partials/errors/403.html'
      })
      .state('500', {
        url: '/500',
        templateUrl: 'templates/partials/errors/500.html'
      });

     $locationProvider.html5Mode(true);
  }
})();
