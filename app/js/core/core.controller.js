(function() {
  'use strict';

  angular.module('app.core').controller('MainController', MainController);

  MainController.$inject = [
    '$scope',
    '$rootScope',
    'EVENTS',
    '$state',
    'toastr',
    'amMoment',
    '$translate',
    'Helper',
    'AuthService'
  ];

  function MainController(
    $scope,
    $rootScope,
    EVENTS,
    $state,
    toastr,
    amMoment,
    $translate,
    Helper,
    AuthService
  ) {
    // this is the parent controller for all controllers.
    // Manages auth login functions and each controller
    // inherits from this controller
    var serverError = function() {
      $state.go('500');
    };

    var badRequest = function() {
      $state.go('400');
    };

    var notFound = function() {
      $state.go('404');
    };

    var translateChangeSuccess = function(event, data) {
      var language = data.language;

      $rootScope.lang = language;
    };

    $scope.changeLanguage = function(langKey) {
      $translate.use(langKey);
      amMoment.changeLocale(langKey);
    };

    $scope.checkLocale = function(locale) {
      return Helper.isLocale(locale);
    };

    var showLoginPage = function() {
      AuthService.removeToken();
      AuthService.removeUserInfo();
      $state.go('login');
    };

    var notAuthorized = function() {
      $state.go('403');
    };

    var loginFailed = function(e, response) {
      toastr.error($translate.instant('bad_request'));
    };

    //listen to events
    $rootScope.$on(EVENTS.serverError, serverError);
    $rootScope.$on(EVENTS.badRequest, badRequest);
    $rootScope.$on(EVENTS.notFound, notFound);
    $rootScope.$on(EVENTS.logoutSuccess, showLoginPage);
    $rootScope.$on(EVENTS.notAuthorized, notAuthorized);
    $rootScope.$on(EVENTS.notAuthenticated, showLoginPage);
    $rootScope.$on(EVENTS.sessionTimeout, showLoginPage);

    $rootScope.$on('$translateChangeSuccess', translateChangeSuccess);
  }
})();
