(function() {
  'use strict';

  angular.module('app.core').factory('$exceptionHandler', exception);

  exception.$inject = ['envService', '$window', '$state'];
  function exception(envService, $window, $state) {
    return function(exception) {
      var env = envService.get();
      if (env === 'development') {
        console.log('exception handled: ' + exception.message);
      }
    };
  }
})();
