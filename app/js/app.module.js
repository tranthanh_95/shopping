(function() {
  "use strict";

  angular.module("app", [
    "app.core",
    "app.auth",
    "app.home",
  ]);
})();
